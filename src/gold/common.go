package gold

import (
    "database/sql"

    _ "github.com/go-sql-driver/mysql"
)

func Cherr(err error) {
    if err != nil {
	panic(err)
    }
}

func OpenDB() (*sql.DB, error) {
    db, err := sql.Open("mysql", "gold:6d8euO5G7451140@/gold")
    Cherr(err)
    return db, err
}

func CloseDB(db *sql.DB) {
    Cherr(db.Close())
}
