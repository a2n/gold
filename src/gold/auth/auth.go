package auth

import (
    "errors"
)

func ValidAuth(token string) (bool, error) {
    if len(token) == 0 {
	return false, errors.New("Empty api token.")
    }

    if token != "35e629c08f46a5e76d11dff416ab5cf0" {
	return false, nil
    }
    return true, nil
}
