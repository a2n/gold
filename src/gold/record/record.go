package record

import (
    "log"
    "net/http"
    "errors"
    "strconv"
    "fmt"

    "gold"
    "gold/auth"
    "gold/user"
)

func Handler(w http.ResponseWriter, r *http.Request) {
    if r.Method != "GET" {
	http.Error(w, "Invalid request.", http.StatusBadRequest)
	log.Println("record.Handler has error, not POST or GET method.")
	return
    }

    switch r.Method {
	case "GET":
	    read(w, r)
    }
}

func Create(id string, price float32) (newId string, ferr error) {
    if len(id) == 0 { 
	id = user.NewID()
    } else {
	if !user.Exist(id) {
	    id = user.NewID()
	}
    }
    newId = id

    db, err := gold.OpenDB()
    gold.Cherr(err)
    defer gold.CloseDB(db)

    q := fmt.Sprintf("INSERT INTO Records VALUES ('%s', NOW(), '%f')", id, price)
    _, err = db.Exec(q)
    if err != nil {
	ferr = err
	log.Printf("record.Create has error, %s", err.Error())
	return newId, ferr
    }

    return newId, nil
}

func read(w http.ResponseWriter, r *http.Request) {
    rs, err := parseRead(r)
    if err != nil {
	log.Println(err.Error())
	http.Error(w, "Invalid request.", http.StatusBadRequest)
	return
    }

    log.Printf("%s (%d - %d)", rs.Identify, rs.Begin, rs.End)
}

type readStruct struct {
    Identify		string
    Begin		int
    End			int
}

func parseRead(r *http.Request) (*readStruct, error) {
    v:= r.URL.Query()

    result, err := auth.ValidAuth(v.Get("a"))
    if err != nil {
	return nil, errors.New("record.parseRead, invalid auth.")
    }

    if result == false {
	return nil, errors.New("record.parseRead, invalid auth.")
    } 

    // Identify
    id := v.Get("i")
    if len(id) == 0 {
	return nil, errors.New("record.parseRead, empty identify.")
    }

    // Begin
    str := v.Get("b")
    if len(str) == 0 {
	return nil, errors.New("record.parseRead, no begin time.")
    }
    begin, err := strconv.Atoi(str)
    if err != nil {
	msg := fmt.Sprintf("record.parseRead has error, %s", err.Error())
	return nil, errors.New(msg)
    }


    // End
    str = v.Get("e")
    if len(str) == 0 {
	return nil, errors.New("record.parseRead, no end time.")
    }
    end, err := strconv.Atoi(str)
    if err != nil {
	msg := fmt.Sprintf("record.parseRead has error, %s", err.Error())
	return nil, errors.New(msg)
    }

    return &readStruct {
	Identify: id,
	Begin: begin,
	End: end,
    }, nil
}
