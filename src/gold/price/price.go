package price

import (
    "log"
    "net/http"
    "strconv"
    "fmt"
    "encoding/json"

    "gold"
    "gold/auth"
    "gold/record"
)

func Handler(w http.ResponseWriter, r *http.Request) {
    if r.Method != "GET" {
	http.Error(w, "Invalid request.", http.StatusBadRequest)
	log.Println("price.Handler has error, not GET http method.")
	return
    }

    read(w, r)
}

type price struct {
    Begin	int
    End		int
}

type jsonOutput struct {
    P		[]out
    I		string
}

func read(w http.ResponseWriter, r *http.Request) {
    v := r.URL.Query()

    // Check auth
    result, err := auth.ValidAuth(v.Get("a"))
    gold.Cherr(err)
    if result == false {
	msg := fmt.Sprintf("price.read auth invalid.")
	log.Println(msg)
	http.Error(w, "Invalid auth.", http.StatusUnauthorized)
	return
    }

    // Begin time
    str := v.Get("b")
    b := 0
    if len(str) > 0 {
	b, err = strconv.Atoi(str)
	if err != nil {
	    msg := fmt.Sprintf("price read, %s", err.Error())
	    log.Println(msg)
	    http.Error(w, "Invalid request.", http.StatusBadRequest)
	    return
	}
    }

    // End time
    str = v.Get("e")
    e := 0
    if len(str) > 0 {
	e, err = strconv.Atoi(str)
	if err != nil {
	    msg := fmt.Sprintf("price read, %s", err.Error())
	    log.Println(msg)
	    http.Error(w, "Invalid request.", http.StatusBadRequest)
	    return
	}
    }

    // Reverse
    if e < b {
	log.Println("Reverse date.")
	http.Error(w, "Invalid request.", http.StatusBadRequest)
	return
    }

    p := price {
	Begin: b,
	End: e,
    }

    os, err := p.read(w)
    if err != nil {
	log.Printf("price.read has error, %s", err.Error)
	return
    }
    var jo jsonOutput
    jo.P = os

    // Id
    id := v.Get("i")
    newid, err := record.Create(id, os[0].P)
    if err != nil {
	return
    }
    if id != newid {
	jo.I = newid
    } else {
	jo.I = id
    }

    jsonstr, err := json.Marshal(jo)
    if err != nil {
	msg := fmt.Sprintf("price.read has error, %s\n", err.Error())
	log.Println(msg)
	http.Error(w, "Ah!", http.StatusInternalServerError)
	return
    }
    w.Write([]byte(jsonstr))
}

type out struct {
    T	    int		`json:"t"`
    P	    float32	`json:"p"`
}

func (p *price) read(w http.ResponseWriter) ([]out, error) {
    db, err := gold.OpenDB()
    gold.Cherr(err)
    defer gold.CloseDB(db)

    q := "SELECT UNIX_TIMESTAMP(created_at), (high + low) / 2 FROM prices"
    if p.Begin != 0 && p.End != 0 {
	q += fmt.Sprintf(" WHERE created_at >= FROM_UNIXTIME(%d) AND created_at <= FROM_UNIXTIME(%d)", p.Begin, p.End)
    } else {
	q += " ORDER BY created_at DESC LIMIT 1"
    }

    rs, err := db.Query(q)
    if err != nil {
	msg := fmt.Sprintf("price.read has error, %s\n", err.Error())
	log.Println(msg)
	http.Error(w, "Ah!", http.StatusInternalServerError)
	return nil, err
    }

    os := make([]out, 0)
    for rs.Next() {
	var t int
	var p float32
	err = rs.Scan(&t, &p)
	if err != nil {
	    msg := fmt.Sprintf("price.read has error, %s\n", err.Error())
	    log.Println(msg)
	    http.Error(w, "Ah!", http.StatusInternalServerError)
	    return nil, err
	}

	os = append(os, out {
	    T: t,
	    P: p,
	})
    }

    return os, nil
}
