package user

import (
    "crypto/md5"
    "fmt"
    "io"
    "time"

    "gold"
)

func NewID() string {
    h := md5.New()
    str := fmt.Sprintf("%d", time.Now().UnixNano())
    io.WriteString(h, str)
    newid := fmt.Sprintf("%x", h.Sum(nil))

    db, err := gold.OpenDB()
    gold.Cherr(err)
    defer gold.CloseDB(db)
    q := fmt.Sprintf("INSERT INTO Users VALUES ('%s')", newid)
    db.Exec(q)

    return newid
}

func Exist(id string) bool {
    db, err := gold.OpenDB()
    gold.Cherr(err)
    defer gold.CloseDB(db)

    q := fmt.Sprintf("SELECT * FROM Users WHERE Id='%s'", id)
    rs, err := db.Query(q)
    return rs.Next()
}
