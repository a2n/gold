package main

import (
    "log"
    "net/http"
    "strings"

    "gold/price"
    "gold/record"
)

func main() {
    f1()
}

func f1() {
    http.HandleFunc("/", handler)
    log.Fatal(http.ListenAndServe(":8000", nil))
}

const (
    Price		= "p"
    Record		 = "r"
)

func handler(w http.ResponseWriter, r *http.Request) {
    w.Header().Add("Access-Control-Allow-Origin", "*")
    w.Header().Add("Access-Control-Allow-Methods","OPTIONS, HEAD, GET, POST, PUT, DELETE",)
    w.Header().Add("Content-Type", "text/html; charset=UTF-8")

    s := strings.Split(r.URL.Path, "/")
    if len(s) <= 2 {
	http.Error(w, "Invalid request.", http.StatusBadRequest)
	log.Println("main.handler, not enough parameters.")
	return
    }

    switch s[1] {
	case Price:
	    price.Handler(w, r)
	case Record:
	    record.Handler(w, r)
    }
}
